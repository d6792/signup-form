import 'normalize.css/normalize.css';
import './App.scss';
import Intro from './components/Intro/Intro';
import FormBox from './components/FormBox/FormBox';
import ThankYou from './components/ThankYou/ThankYou';
import React, { useState } from 'react';


function App() {
  const myTitle = 'Learn to code by watching others';
  const myPhrases = [
    'See how experienced developers solve problems in real-time.',
    'Watching scripted tutorials is great, but understanding how developers think is invaluable.',
  ];
  const callbackFn = (name) => {
    setChildSubmit(true);
    setFirstname(name);
  }

  const [childSubmit, setChildSubmit] = useState(false);
  const [firstname, setFirstname] = useState("");


  return (
    <div className="app">
      <Intro title={myTitle} phrases={myPhrases} />
      <div>{!childSubmit ? <FormBox onSubmit={(callbackFn)} /> : <ThankYou firstname={firstname}/>}</div>
    </div>
  );
}

export default App;
