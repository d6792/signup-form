import './ThankYou.scss';

export default function ThankYou(props) {

    return <>
        <div className="form-box">
            <div className="tryit">
                <strong>
                    Try it free 7 days 
                </strong>
                then $20/mo. thereafter
            </div>

            <form>

                <p className="obrigado"> Obrigado, {props.firstname} pelo submit!</p>
            </form>
        </div>
    </>
}