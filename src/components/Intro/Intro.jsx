import './Intro.scss';

export default function Intro(props) {
    return <>
        <div className="intro">
            <h1>{props.title}</h1>
            <p>
                {props.phrases.join(' ')} 
            </p>
        </div>
    </>
}