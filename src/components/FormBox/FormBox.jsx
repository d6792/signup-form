import './FormBox.scss';
import React, { useState, useMemo, useCallback } from 'react';


export default function Formbox(props) {

    const [submitted, setSubmitted] = useState(false);
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const submitForm = (e) => {
        e.preventDefault();
        setSubmitted(true);        

        if (validateForm().length === 0) props.onSubmit(firstname);
    }

    const validateForm = useCallback(() => {
        const arrErrors = [];
        const emailRgx = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/g;
        const passRgx = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/g;
        if (firstname.length === 0) arrErrors.push('firstname');
        if (lastname.length === 0) arrErrors.push('lastname');
        if (!emailRgx.test(email)) arrErrors.push('email');
        if (!passRgx.test(password)) arrErrors.push('password');
        return arrErrors;
    }, [firstname, lastname, email, password]);

    const errors = useMemo(() => {
        if (submitted) return validateForm();
        return [];
    },[submitted, validateForm]);


    return <>
        <div className="form-box">
            <div className="tryit">
                <strong>
                    Try it free 7 days 
                </strong>
                then $20/mo. thereafter
            </div>

            <form onSubmit={submitForm}>

                <div className={'form-input ' + (errors.includes('firstname') ? 'error' : '') }>
                    <input type="text" placeholder="First name" value={firstname} onChange={e => setFirstname(e.target.value)} />
                    {errors.includes('firstname') ? <div className='error-message'>First Name cannot be empty</div> : null}
                </div>

                <div className={'form-input ' + (errors.includes('lastname') ? 'error' : '') }>
                    <input type="text" placeholder="Last name" value={lastname} onChange={e => setLastname(e.target.value)} />
                    {errors.includes('lastname') ? <div className='error-message'>Last Name cannot be empty</div> : null}
                    </div>

                <div className={'form-input ' + (errors.includes('email') ? 'error' : '') }>
                    <input type="email" placeholder="Email Address" value={email} onChange={e => setEmail(e.target.value)} />
                    {errors.includes('email') ? <div className='error-message'>Email cannot be empty</div> : null}
                </div>

                <div className={'form-input ' + (errors.includes('password') ? 'error' : '') }>
                    <input type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} />
                    {errors.includes('password') ? <div className='error-message'>Password must has minimum eight characters, at least one letter and one number</div> : null}
                    </div>

                <div>
                    <button type='submit'>CLAIM YOUR FREE TRIAL</button>
                    <div className="byclicking">By clicking the button, you are agreeing to our <a  href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"  rel="noreferrer" target="_blank">Terms and Services</a></div>
                </div>
            </form>
        </div>
    </>
}